import time
import json
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import config
from pymongo import MongoClient
client = mqtt.Client()
mongo = MongoClient(config.database,config.portDB)

def on_message_print(client, userdata, message):
	print("%s %s" % (message.topic, message.payload))
	
	carVec = message.topic.split("/")
	VIN = carVec[1]
	MAC = carVec[2]
	code = carVec[3]
	value = message.payload
	addMsgDb(VIN,code,value)
	myfile = str(VIN)+".txt"
	f = open(myfile,"a")
	f.write(json.dumps({ code : value}))
	f.write("\n")
def addMsgDb(collection,code,value):
	db = mongo['mqttDB']
	collection = db[collection]

	post = {code:value,
			"date":datetime.datetime.utcnow()}
	print post
	time1 = datetime.datetime.now()
	collection.insert_one(post)
	time2 = datetime.datetime.now()
	delta = time2-time1
	print "time for writing data in db:",delta.total_seconds()*1000
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    #client.subscribe("$SYS/broker/publish/messages/dropped/# ")
    #client.subscribe("$SYS/broker/publish/messages/received/#")
    client.subscribe("$SYS/broker/uptime/#")
    client.subscribe("$SYS/broker/messages/received/#")
    


def on_message(client, userdata, message):
	print("message received %s " % str(message.payload.decode("utf-8")))
	print("message topic= %s" % message.topic)
def main():
	#set connection
	client.on_connect = on_connect
	
	client.on_message = on_message

	print("Connecting to broker")

	client.connect(config.host_connection,config.port)

	client.on_connect
	
	client.loop_start()
	subscribe.callback(on_message_print, "Car_fleet/#", hostname=config.host_connection)
	#client.loop_forever()

	


if __name__ == "__main__":
	main() 	
